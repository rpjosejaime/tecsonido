<?php

namespace ArangoDBClient;

require __DIR__ . '/init.php';

/* set up some example statements */
$statements = [
    'for u in users return u'                                       => null,
    'for u in users filter u.id == @id return u'                    => ['id' => 6],
    'for u in users filter u.id == @id && u.name != @name return u' => ['id' => 1, 'name' => 'fox'],
];

$statements = [
'FOR	u	IN	poliza FILTER	u.predio_tipo	==	@id RETURN	u' => ['id' => 2]
];

try {
    $connection = new Connection($connectionOptions);

    foreach ($statements as $query => $bindVars) {
        $statement = new Statement($connection, [
                'query'     => $query,
                'count'     => true,
                'batchSize' => 1,
                'bindVars'  => $bindVars,
                'sanitize'  => true,
            ]
        );

        print $statement . "\n\n";

        $cursor = $statement->execute();
        echo "<hr>";
        $row = $cursor->current();
        var_dump($cursor->current());
        echo "<hr>";
        var_dump($cursor->getMetadata()["result"]);
        $res = $cursor->getMetadata()["result"];
        echo "<hr>";
        var_dump($res);
        echo "<hr>";
        echo $res[0]["folio"];
       // var_dump($cursor->getName());
        exit;
        break;
    }
} catch (ConnectException $e) {
    print $e . PHP_EOL;
} catch (ServerException $e) {
    print $e . PHP_EOL;
} catch (ClientException $e) {
    print $e . PHP_EOL;
}
