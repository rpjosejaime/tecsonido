<?php

namespace ArangoDBClient;

// require __DIR__ . '/../arangodb/examples/init.php';

require __DIR__ . '/../arangodb/autoload.php';
//require __DIR__ . '/arangodb/autoload.php';


/**
 * Description of arangodb
 *
 * @author govani
 */
class ArangoDB {

    public function __construct() {
        $connectionOptions = [
            ConnectionOptions::OPTION_DATABASE => '_system', // '_system',               // database name
// normal unencrypted connection via TCP/IP
            //ConnectionOptions::OPTION_ENDPOINT => 'tcp://localhost:8529', // endpoint to connect to
            ConnectionOptions::OPTION_ENDPOINT => 'tcp://arangodb.jimvps.tk:8529', // endpoint to connect to
// // connection via SSL
// ConnectionOptions::OPTION_ENDPOINT        => 'ssl://localhost:8529',  // SSL endpoint to connect to
// ConnectionOptions::OPTION_VERIFY_CERT     => false,                   // SSL certificate validation
// ConnectionOptions::OPTION_ALLOW_SELF_SIGNED => true,                  // allow self-signed certificates
// ConnectionOptions::OPTION_CIPHERS         => 'DEFAULT',               // https://www.openssl.org/docs/manmaster/apps/ciphers.html
// // connection via UNIX domain socket
// ConnectionOptions::OPTION_ENDPOINT        => 'unix:///tmp/arangodb.sock',  // UNIX domain socket
            ConnectionOptions::OPTION_CONNECTION => 'Keep-Alive', // can use either 'Close' (one-time connections) or 'Keep-Alive' (re-used connections)
            ConnectionOptions::OPTION_AUTH_TYPE => 'Basic', // use basic authorization
// authentication parameters (note: must also start server with option `--server.disable-authentication false`)
            ConnectionOptions::OPTION_AUTH_USER => 'root', // user for basic authorization
            ConnectionOptions::OPTION_AUTH_PASSWD => 'nosql', // password for basic authorization
            ConnectionOptions::OPTION_TIMEOUT => 30, // timeout in seconds
            //    ConnectionOptions::OPTION_TRACE => $traceFunc, // tracer function, can be used for debugging
            ConnectionOptions::OPTION_CREATE => false, // do not create unknown collections automatically
            ConnectionOptions::OPTION_UPDATE_POLICY => UpdatePolicy::LAST, // last update wins
        ];

        $this->connection = new Connection($connectionOptions);
    }

    public function traceFunc($type, $data) {
        print 'TRACE FOR ' . $type . PHP_EOL;
        var_dump($data);
    }

    public function execute_sentencia($query, $batchSize = 100) {

        // $query = 'FOR x IN firstCollection RETURN x._key';
        $statement = new Statement(
                $this->connection, array(
                    "query" => $query,
                    "count" => true,
                    "batchSize" => $batchSize,
                    "sanitize" => true
                )
        );

        $cursor = $statement->execute();
        $resultingDocuments = array();

//         $res = $cursor->getAll();
//         var_dump($res);
        $res = $cursor->current();
        return $cursor->getMetadata()["result"];

//return $res;

        /*
          foreach ($cursor as $key => $value) {
          $resultingDocuments[$key] = $value;
          }
         */
        // var_dump($resultingDocuments[0]["folio"]);
    }

    public function execute_sentencia_insert($query, $document) {

        // $query = 'FOR x IN firstCollection RETURN x._key';
        $statement = new Statement(
                $this->connection, array(
            "query" => $query,
            "count" => true,
            "batchSize" => 1,
            "sanitize" => true,
            "bindVars" => array("doc" => $document)
                )
        );

        $cursor = $statement->execute();

    }


}
