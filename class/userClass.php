<?php
require_once 'arangodb.php';

class userClass extends ArangoDBClient\ArangoDB
{
     public function userLogin($usernameEmail,$password,$secret)
     {
          $hash_password= hash('sha256', $password);
          $stmt = "FOR u	IN reproduccion_users "
                  . " FILTER (u.username == '${usernameEmail}' || u.email == '${usernameEmail}') && u.password=='${hash_password}' "
                  . " LIMIT 0, 2000 "
                  . " RETURN u";

          $stmt=$this->execute_sentencia($stmt,  $batchSize = 100);
          $count=count($stmt);

          if($count)
          {
               foreach ($stmt as $r):
                 $uid=$r["uid"];
               endforeach;

               $_SESSION['uid']=$uid;
               $_SESSION['google_auth_code']=$google_auth_code;

               return true;
          }
          else
          {
               return false;
          }
     }


     /* User Details */
     public function userDetails($uid)
     {
        $stmt = "FOR u	IN reproduccion_users "
                . " FILTER u.uid == '${uid}' "
                . " LIMIT 0, 2000 "
                . " RETURN u";

        $stmt=$this->execute_sentencia($stmt,  $batchSize = 100);

        return $stmt;
     }

     public function bitacora_inicio($data){
        foreach ($data as $r):
          $uid=$r["uid"];
          $username=$r["username"];
        endforeach;

        $resul = "FOR u	IN log_users "
                . " FILTER u.fecha_logout == '' && u.uid =='$uid' "
                . " LIMIT 0, 2000 "
                . " RETURN u";

        $resul=$this->execute_sentencia($resul,  $batchSize = 100);
        $count=count($resul);

        if($count)
        {}
          else{
        date_default_timezone_set('America/Monterrey');
        $fecha_login=date('d-m-y / h:i:s A');
        $ip_add=$_SERVER["REMOTE_ADDR"];

        $stmt = "INSERT {uid: \"$uid\",
                            username: \"$username\",
                            ip_address: \"$ip_add\",
                            fecha_login: \"$fecha_login\",
                            fecha_logout: \"\"
                          } IN log_users";

        $stmt=$this->execute_sentencia($stmt,  $batchSize = 100);
        }
     }

     public function bitacora_fin($data){
       date_default_timezone_set('America/Monterrey');
       $fecha_logout=date('d-m-y / h:i:s A');

       foreach ($data as $r):
         $uid=$r["uid"];
       endforeach;

       $resul = "FOR u	IN log_users "
               . " FILTER u.fecha_logout == '' && u.uid =='$uid' "
               . " LIMIT 0, 2000 "
               . " RETURN u";
       $resul=$this->execute_sentencia($resul,  $batchSize = 100);


       foreach ($resul as $r):
          $key=$r["_key"];
       endforeach;

       $stmt = "UPDATE \"$key\" WITH {
                      fecha_logout: \"$fecha_logout\"
                  } IN log_users";

       $stmt=$this->execute_sentencia($stmt,  $batchSize = 100);



     }

     public function reiniciar (){
       $stmt = "FOR u IN reproduccion_sonidos

                UPDATE u WITH { reproducciones:0 } IN reproduccion_sonidos";

       $stmt=$this->execute_sentencia($stmt,  $batchSize = 100);

     }

     public function error_login($data,$error){
       date_default_timezone_set('America/Monterrey');
       $fecha=date('d-m-y / h:i:s A');

       $stmt = "INSERT {   username: \"$data\",
                           error: \"$error\",
                           fecha: \"$fecha\"
                         } IN log_error";

       $this->execute_sentencia($stmt,  $batchSize = 100);

     }

     /* User Registration */
     public function userRegistration($username,$password,$email,$name,$secret)
     {

       $stmt = "FOR u	IN reproduccion_users "
               . " FILTER u.username == '${username}' || u.email == '${email}' "
               . " LIMIT 0, 2000 "
               . " RETURN u";

       $stmt=$this->execute_sentencia($stmt,  $batchSize = 100);
       $count=count($stmt);

       if($count<1)
       {
         $pass=hash('sha256', $password);
         $stmt = "INSERT {
                             username: \"$username\",
                             password: \"$pass\",
                             email: \"$email\",
                             name: \"$name\",
                             username: \"$username\",
                             google_auth_code: \"$secret\"

                           } IN users";

       $stmt=$this->execute_sentencia($stmt,  $batchSize = 100);

       $stmt = "FOR u IN reproduccion_users "
               . " FILTER u.username == '${username}' "
               . " LIMIT 1"
               . " RETURN u";

       $stmt=$this->execute_sentencia($stmt,  $batchSize = 100);


       foreach ($stmt as $r):
         $uid=$r["_key"];
       endforeach;


       $_SESSION['uid']=$uid;
       return true;
       }
      else
       {

       return false;
       }
     }

     public function getRealIP() {

          if (isset($_SERVER["HTTP_CLIENT_IP"]))
          {
             return $_SERVER["HTTP_CLIENT_IP"];
          }
          elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
          {
             return $_SERVER["HTTP_X_FORWARDED_FOR"];
          }
          elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
          {
             return $_SERVER["HTTP_X_FORWARDED"];
          }
          elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
          {
             return $_SERVER["HTTP_FORWARDED_FOR"];
          }
          elseif (isset($_SERVER["HTTP_FORWARDED"]))
          {
             return $_SERVER["HTTP_FORWARDED"];
          }
          else
          {
             return $_SERVER["REMOTE_ADDR"];
          }

      }

      public function get_sonido ($sonido) {
        $query = "FOR u	IN reproduccion_sonidos "
		    . " FILTER u.nombre == '${sonido}' "
                . " LIMIT 0, 2000 "
                . " RETURN u";
        return $this->execute_sentencia($query,  $batchSize = 100);
     }

    public function get_log_users() {
        $query = "FOR u	IN log_users "
                . " LIMIT 0, 2000 "
                . " RETURN u";
        return $this->execute_sentencia($query,  $batchSize = 100);
    }

    public function get_log_error() {
        $query = "FOR u	IN log_error "
                . " LIMIT 0, 2000 "
                . " RETURN u";
        return $this->execute_sentencia($query,  $batchSize = 100);
    }


    public function get_historial() {
        $query = "FOR u	IN reproduccion_sonidos "
                . "SORT u.codigo"
                . " LIMIT 0, 2000 "
                . " RETURN u";
        return $this->execute_sentencia($query,  $batchSize = 100);
    }

    public function get_todo() {
        $query = "FOR u	IN reproduccion_registro "
                . " FILTER u.key_sonido == '19575'  "
                . "SORT u.fecha"
                . " LIMIT 0, 2000 "
                . " RETURN u";
        return $this->execute_sentencia($query,  $batchSize = 100);
    }
}
?>
