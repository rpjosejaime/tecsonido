<?php
session_start();
include('class/userClass.php');
$userobj = new userClass();

define("BASE_URL", "https://tecsonido.jimvps.tk/"); // Eg. http://yourwebsite.com

$usertime=$userobj->userDetails($_SESSION['uid']);
foreach ($usertime as $r):
  $tiempo=$r["time"];
endforeach;

// Máxima duración de sesión activa en hora
//define( 'MAX_SESSION_TIEMPO', 3600 * 1 );
define( 'MAX_SESSION_TIEMPO', $tiempo );

// Controla cuando se ha creado y cuando tiempo ha recorrido
if ( ($tiempo > 0) && isset( $_SESSION[ 'ULTIMA_ACTIVIDAD' ] ) &&
     ( time() - $_SESSION[ 'ULTIMA_ACTIVIDAD' ] > MAX_SESSION_TIEMPO ) ) {

    // Si ha pasado el tiempo sobre el limite destruye la session
    destruir_session();
}

$_SESSION[ 'ULTIMA_ACTIVIDAD' ] = time();


// Función para destruir y resetear los parámetros de sesión
function destruir_session() {
  $session_uid='';
  $session_googleCode='';
  $_SESSION['uid']='';
  $_SESSION['googleCode']='';
  if(empty($session_uid) && empty($_SESSION['uid']))
  {
  $url=BASE_URL.'index.php';

  header("Location: $url");
  }
}

?>
